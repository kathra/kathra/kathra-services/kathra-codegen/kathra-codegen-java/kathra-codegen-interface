/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */

package org.kathra.codegen;

import org.kathra.codegen.model.CodeGenTemplate;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.kathra.KathraAuthRequestHandlerImpl;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.model.rest.RestBindingMode;
import static org.apache.camel.model.rest.RestParamType.*;

@ContextName("Codegen")
public class CodegenApi extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        // configure we want to use servlet as the component for the rest DSL
        // and we enable json binding mode
        restConfiguration().component(org.kathra.iface.KathraRequestHandler.HTTP_SERVER)
        // use json binding mode so Camel automatic binds json <--> pojo
        .bindingMode(RestBindingMode.off)
        // and output using pretty print
        .dataFormatProperty("prettyPrint", "true")
        .dataFormatProperty("json.in.disableFeatures", "FAIL_ON_UNKNOWN_PROPERTIES")
        // setup context path on localhost and port number that netty will use
        .contextPath("/api/v1")
        .port("{{env:HTTP_PORT:8080}}")
        .componentProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))
        .endpointProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))
        .consumerProperty("chunkedMaxContentLength", String.valueOf( 1400 * 1024 * 1024))

        // add swagger api-doc out of the box
        .apiContextPath("/swagger.json")
        .apiProperty("api.title", "Kathra CodeGenerator Manager")
        .apiProperty("api.version", "1.2.0")
        .apiProperty("api.description", "Codegen")
        // and enable CORS
        .apiProperty("cors", "true")
        .enableCORS(true).corsAllowCredentials(true)
        .corsHeaderProperty("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type," +
                "Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");

        rest()

        .post("/generateFromTemplate").type(CodeGenTemplate.class).consumes("application/json").produces("application/octet-stream")
            .description("Generate archive from template")
                .param()
                    .required(true)
                    .name("CodeGenTemplate")
                    .type(body)
                    .description("CodeGenTemplate to generate code")
                .endParam()
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .unmarshal().json(JsonLibrary.Gson,CodeGenTemplate.class)
                .bean(KathraAuthRequestHandlerImpl.class,"handleRequest")
                .to("bean:CodegenController?method=generateFromTemplate(${body})")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
            .endRest()

        .get("/templates").outType(CodeGenTemplate[].class).consumes("application/json").produces("application/json")
            .description("Get all templates for codegen generation")
            .route().onException(Exception.class)
                    .bean(KathraAuthRequestHandlerImpl.class,"handleException")
                    .bean(KathraAuthRequestHandlerImpl.class,"deleteSession")
                    .handled(true).stop().end()
                .bean(KathraAuthRequestHandlerImpl.class,"handleRequest")
                .to("bean:CodegenController?method=getTemplates")
                .bean(KathraAuthRequestHandlerImpl.class,"postProcessResponse")
                .marshal().json(JsonLibrary.Gson)
            .endRest();
    }
}